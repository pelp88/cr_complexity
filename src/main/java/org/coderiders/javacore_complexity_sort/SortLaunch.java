package org.coderiders.javacore_complexity_sort;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class SortLaunch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter array length: ");
        int n = in.nextInt();
        if (n <= 0) {
            System.out.println("Length can't be less than 1!");
            System.exit(1);
        }
        Vector<Integer> randomArray = new Vector<>();

        for (int i = 0; i <= n - 1; i++) {
            randomArray.add(new Random().nextInt(100));
        }

        System.out.print("Choose sort algorithm (1 - bubble, 2 - selection, 3 - insertion, 4 - quick): ");
        int type = in.nextInt();
        Sort sort = null;
        switch (type) {
            case 1: sort = new BubbleSort(randomArray); break;
            case 2: sort = new SelectionSort(randomArray); break;
            case 3: sort = new InsertionSort(randomArray); break;
            case 4: sort = new QuickSort(randomArray); break;
            default: System.out.print("Incorrect value!"); System.exit(1);
        }
        System.out.println("Results (ascending): ");
        for (Integer item : sort.result()) {
            System.out.print(item);
            System.out.print(" ");
        }

        System.out.println(" ");

        System.out.println("Results (descending): ");
        for (Integer item : sort.resultDescending()) {
            System.out.print(item);
            System.out.print(" ");
        }
    }
}
