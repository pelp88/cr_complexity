package org.coderiders.javacore_complexity_sort;

import java.util.Collections;
import java.util.Vector;

public abstract class Sort {
    public Vector<Integer> array;

    Sort(Vector<Integer> arr) {
        this.array = arr;
    }

    public Vector<Integer> result() {
        return this.array;
    }

    public Vector<Integer> resultDescending() {
        Vector<Integer> temp = this.array;
        Collections.reverse(temp);
        return temp;
    }
}
