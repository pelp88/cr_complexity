package org.coderiders.javacore_complexity_sort;

import java.util.Vector;

public class SelectionSort extends Sort{
    public SelectionSort(Vector<Integer> arr) {
        super(arr);
        sort();
    }

    private void sort() { // реализация алгоритма сортировки выбором
        int size = this.array.size();

        // Последовательно смещать нижнюю границу массива
        for (int i = 0; i < size; i++)
        {
            // Индекс минимального элемента текущего массива
            int minIndex = i;
            for (int j = i + 1; j < size; j++)
                if (this.array.get(j) < this.array.get(minIndex))
                    minIndex = j;

            // Поменять местами минимальный элемент с текущим
            int temp = this.array.get(minIndex);
            this.array.set(i, this.array.get(minIndex));
            this.array.set(i, temp);
        }
    };
}
