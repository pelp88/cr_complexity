package org.coderiders.javacore_complexity_sort;

import java.util.Vector;

public class BubbleSort extends Sort{
    public BubbleSort(Vector<Integer> arr) {
        super(arr);
        sort();
    }

    private void sort() { // реализация алгоритма сортировки bubblesort
        int end = this.array.size() - 1;

        // итерируем через входной массив
        for(int i = 0; i < end; i++)
        {
            // соритируем часть элементов
            for(int j = 0; j < end - i; j++)
            {
                if(this.array.get(j) > this.array.get(j + 1))
                {
                    // меняем местами элементы, если текущий больше следующего
                    int temp = this.array.get(j);
                    this.array.set(j, this.array.get(j + 1));
                    this.array.set(j + 1, temp);
                }
            }
        }
    };
}
