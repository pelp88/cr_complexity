package org.coderiders.javacore_complexity_sort;

import java.util.Vector;

public class InsertionSort extends Sort{
    public InsertionSort(Vector<Integer> arr) {
        super(arr);
        sort();
    }

    private void sort() { // реализация алгоритма сортировки вставками
        int end = this.array.size();

        // итерация через массив
        for (int i = 1; i < end; ++i) {
            int elem = this.array.get(i);

            // переместить все элементы подмассива больше чем elem, на одну позицию вперед
            int j = i - 1;
            while (j >= 0 && this.array.get(j) > elem) {
                this.array.set(j + 1, this.array.get(j));
                j = j - 1;
            }
            this.array.set(j + 1, elem);
        }
    }
}
