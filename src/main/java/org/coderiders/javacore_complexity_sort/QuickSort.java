package org.coderiders.javacore_complexity_sort;

import java.util.Vector;

public class QuickSort extends Sort{
    public QuickSort(Vector<Integer> arr) {
        super(arr);
        sort();
    }

    private void sort() {
        int end = this.array.size() - 1;

        this.quickSort(0, end);
    }

    private void quickSort(int start, int end) { // реализация алгоритма QuickSort
        if (start < end) {
            int partitionIndex = getPartitionIndex(start, end);

            quickSort(start, partitionIndex - 1);
            quickSort(partitionIndex + 1, end);
        }
    }

    private int getPartitionIndex(int start, int end) {
        int pivot = this.array.get(end);
        int i = start - 1;

        for (int j = start; j < end; j++) {
            if (this.array.get(j) <= pivot) {
                i++;
                int temp = this.array.get(i);
                this.array.set(i, this.array.get(j));
                this.array.set(j, temp);
            }
        }
        int temp = this.array.get(i + 1);
        this.array.set(i, end);
        this.array.set(end, temp);

        return i + 1;
    }
}
