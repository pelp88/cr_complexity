package org.coderiders.javacore_complexity_search;

import java.util.Vector;

public abstract class Search {
    public Vector<Integer> array;
    public Integer resultIndex;

    Search(Vector<Integer> arr) {
        this.array = arr;
    }

    public int result() {
        return resultIndex;
    }
}
