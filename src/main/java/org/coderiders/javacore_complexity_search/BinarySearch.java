package org.coderiders.javacore_complexity_search;

import org.coderiders.javacore_complexity_sort.SelectionSort;

import java.util.Vector;

public class BinarySearch extends Search{
    BinarySearch(Vector<Integer> arr, Integer elem) {
        super(arr);
        this.resultIndex = search(elem);
    }

    private int search(int elem) {
        int start = 0;
        int end = this.array.size();
        int middleElem = end / 2;

        while (start <= end) {
            if (this.array.get(middleElem) < elem){
                start = middleElem + 1;
            } else if (this.array.get(middleElem) == elem){
                return middleElem;
            } else {
                end = middleElem - 1;
            }
            middleElem = (start + end) / 2;
        }
        return -1;
    }
}
