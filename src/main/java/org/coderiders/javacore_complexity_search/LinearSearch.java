package org.coderiders.javacore_complexity_search;

import java.util.Objects;
import java.util.Vector;

public class LinearSearch extends Search{
    LinearSearch(Vector<Integer> arr, Integer elem) {
        super(arr);
        this.resultIndex = search(elem);
    }

    private int search(Integer elem) {
        for (int i = 0; i <= this.array.size(); i++) {
            if (Objects.equals(this.array.get(i), elem)) {
                return i;
            }
        }
        return -1; // я не понимаю почему оно падает при значении которого нет в массиве и не возв. -1
    }
}
