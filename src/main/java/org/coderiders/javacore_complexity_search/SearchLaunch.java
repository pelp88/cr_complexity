package org.coderiders.javacore_complexity_search;

import org.coderiders.javacore_complexity_sort.*;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class SearchLaunch {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter array length: ");
        int n = in.nextInt();
        if (n <= 0) {
            System.out.println("Length can't be less than 1!");
            System.exit(1);
        }
        Vector<Integer> randomArray = new Vector<>();

        for (int i = 0; i <= n - 1; i++) {
            randomArray.add(new Random().nextInt(100));
        }

        System.out.print("Choose search algorithm (1 - linear, 2 - binary): ");
        int type = in.nextInt();
        if (type == 2) {
            System.out.println("Sorting array...");
            randomArray = new SelectionSort(randomArray).result();
        }
        System.out.println("Array: ");
        for (Integer item : randomArray) {
            System.out.print(item);
            System.out.print(" ");
        }
        System.out.println(" ");

        System.out.print("Enter element you want to search: ");
        Integer element = in.nextInt();

        Search search = null;
        switch (type) {
            case 1: search = new LinearSearch(randomArray, element); break;
            case 2: search = new BinarySearch(randomArray, element); break;
            default: System.out.print("Incorrect value!"); System.exit(1);
        }

        System.out.print("Index is ");
        System.out.print(search.result());
    }
}
